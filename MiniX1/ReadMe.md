## MiniX1 - Preface: Getting started

[_Click to view my **miniX1**_](https://oliviasofie1.gitlab.io/aestetiskprogrammering/MiniX1/)

[_Click to view my my **code**_](https://gitlab.com/oliviasofie1/aestetiskprogrammering.git)



### **What have I produced for my MiniX1?**
For my MiniX1 I decided to portray and create a sunrise with a cozy and aesthetic landscape. I have tried to use some different colors,  various of geometric shapes and some motion to created a rising sun and  idyllic landscape with a welcoming feeling. The objects, that I have created to make the landscape is three mountains with windmills on two of them, clouds, a little house with small flowers next to it  and a sun, which change the color of the sky slightly as it rises.

The main focus for me was to make a landscape, which people would find interesting. Other then that, I was wanted to make alive. Therefore, I decided already before I started programming, that I wanted to make a sunrise to make something that's in motion. I tried to play a bit with different syntax to make the sun rise up over the landscape, and therefore I ended up with using "let" and  the reserved keyword "If" ("If statements). 

You can see the result of my landscape and the rising sun in the video here:

![](MINIX1.mp4)

I used the "let" function, before creating the setup() function, which allowed me to declare a set of variables, "red" "green",  "blue" and "sun", these  I was going to use later in the code. In setup() I created a canvas, which was going to be the base of the landscape. After this I created the draw() function, which is containing the main code for drawing the different elements on the canvas. Inside the draw() function, I used if-else statements, which changes the background color based on the position of the sun (sun variable). If the suns position is lees than or equal to 120, a light blue background is displayed. Otherwise, the background color changes gradually as the sun rises, with the RGB values increasing slightly, this is creating a visual effect, because it's a transition from night to day. The sun is created with two concentric circles, one larger and one smaller, to create the effect of brightness. The position of the sun decreases over time, by using sun--, which simulate it moving upward. Other then this, I also used various shapes and colors to create the different elements in the landscape. I did this with the help of the following syntax: 


- fill()
- noStroke()
- stroke()
- storkeweight()
- textSize()
- text()

For the different shapes I have used these:
- rect()
- ellipse()
- circle()
- triangle()
- line()


### **How would I describe my first independent coding experience?**
I would describe my first independent coding experience as very interesting and a lot of fun, because p5,js have been pretty easy to figure out and play around with. Under the coding I have also been a bit challenged, because I didn't really work in the start, but after a bit of time I figured everything out, which was a big success for me. For my first MiniX the focus was just to get to now p5.js and play a bit around with the shapes, colors and a bit of motion. Therefore I'm happy with the result,. I think, that I have been learning a lot from doing this MiniX, but I wish that I could do even more then this, so I'm very excited to get better and better at programming, and get a better understanding for it.  


### **How is the coding process different from, or similar to, reading and writing text?**
I have been thinking a lot, about this under the coding process, because I have figured that you will not, like in Microsoft Word, get told if the grammar is incorrect. Therefore it is very important to be focused when writing the code, because just one fault in the grammar dictates the structure. To ensure that the program is running correctly, you have to read that you have spelt and structured everything in the right way. In this way, coding can actually be similar to how a writer, because they need to follow grammar rules, where programmer on the other hand have to follow syntax rules to create a functional code. 

I have doing the last weeks been trying a lot of times, that my code didn't work right, because I have done something wrong in the syntax (sometimes very small mistakes). Therefore, I have over the last weeks learned a lot about the two programming languages JavaScript and p5.js are working and tied together. I have learned this, because I have been coding thing myself but also reading a lot of other peoples codes, which have help me a lot. Therefore I will keep on doing this, so I can get an even better understanding, so I can code even better and nicer things in the near future.

### **Reference**
- https://p5js.org/reference/#/p5/let
- https://p5js.org/reference/#/p5/fill
- https://p5js.org/reference/#/p5/strokeWeight
- https://p5js.org/reference/#/p5/stroke
- https://p5js.org/reference/#/p5/if-else
- https://p5js.org/reference/#/p5/text
- https://p5js.org/reference/#/p5/background
- https://p5js.org/reference/#/p5/noStroke
- https://p5js.org/reference/#/p5/ellipse
- https://p5js.org/reference/#/p5/circle
- https://p5js.org/reference/#/p5/line
- https://p5js.org/reference/#/p5/rect
- https://p5js.org/reference/#/p5/triangle
- https://p5js.org/reference/#/p5/textSize
- https://www.rapidtables.com/web/color/RGB_Color.html


### **Code, that I have looked at as inspiration:**
https://editor.p5js.org/dhabialhosani/sketches/fS_CtqR5a



