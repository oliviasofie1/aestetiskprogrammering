//Declaring global variables (which will get used later):
let red=0;
let green=0;
let blue=0;
let sun=400; //starting position of the sun (y-coordinates = 400)

//runs once in the program start
function setup(){
    createCanvas(1000, 400);
   }
  
  //Function draw is called for each running frmae by default 60 times pr. second (frameRate)
   function draw(){
    // Adjusting background color based on the position of the sun
    if (sun<=120){
      background(173, 216, 230); // lightblue background after the sun has come up
    }else{
      background(red+=5, green+=2, blue+=1); //Gradually chaning to lighter colors
    }
      

     //Now I make the sun:
     //The outside of the sun
     noStroke();
     fill(255, 175, 10, 50);

     if (sun>120){
      circle (900, sun--, 175);
     }else if (sun==120){
      circle(900, sun, 175);
     }

     // The inside of the sun
     noStroke()
     fill(255, 160, 10, 100);

     if(sun>120){
      circle(900, sun--, 115);
     } else if (sun==120){
      circle (900, sun, 115); //sun is not decremented (--) = ensures that the sun stops moving
     }
    
      // using and calling different p5.js functions to create the landscape on the canvas
     // the mountains 
     strokeWeight(4);
     stroke(51, 100);
     fill(85, 65, 36);
     triangle(250, 400, 500, 200, 750, 400); //(x1, y1, x2, y2, x3, y3)
     fill(160, 100, 10);
     triangle(0, 400, 250, 210, 500, 400);
     fill(160, 100, 10);
     triangle(500, 400, 750, 175, 1000, 400);

     // The little pink house:
     //The house:
     stroke(0);
     strokeWeight(1);
     fill(255, 204, 204);
     rect(646, 330, 70, 70); //(x, y, w, h)

     //Chimney:
     stroke(0);
     strokeWeight(1);
     fill(153, 76, 0);
     rect(696, 290, 16, 25);

     // Roof of the house:
     stroke(0);
     strokeWeight(1);
     fill(255, 178, 102);
     triangle(630, 330, 730, 330, 680, 290);

     // Door:
     stroke(204, 255, 255);
     strokeWeight(1);
     fill(204, 255, 255);
     rect(671, 364, 20, 35);

     // text over the door:
      textSize(10);
      fill(255, 204, 204);
      stroke(204, 255, 255);
      strokeWeight(2);
      text('Welcome', 661, 352);

     // Door knob 
     noStroke();
     fill(102, 178, 255);
     ellipse(676, 380, 5, 5) //(x, y, w, h)

      // Windmill on mountain 1:
      stroke(0, 150);
      strokeWeight(5);
      line(210, 195, 210, 238);
      stroke(0, 150);
      strokeWeight(1);
      fill(255);
      ellipse(210, 180, 7, 20);
      ellipse(220, 190, 20, 7);
      ellipse(200, 190, 20, 7);
      ellipse(210, 200, 7, 20);

      // Windmill on mountain 2:
      stroke(0, 150);
      strokeWeight(5);
      line(450, 195, 450, 238);
      stroke(0, 150);
      strokeWeight(1);
      fill(255);
      ellipse(450, 180, 7, 20);
      ellipse(460, 190, 20, 7);
      ellipse(440, 190, 20, 7);
      ellipse(450, 200, 7, 20);

     // Sky 1:
      noStroke();
      fill(254, 235, 201);
      ellipse(100, 100, 100, 55);
      ellipse(150, 110, 100, 55);
      ellipse(210, 100, 100, 55);
      ellipse(160, 85, 100, 55);

      // Sky 2:
      noStroke();
      fill(254, 235, 201);
      ellipse(390, 75, 100, 55);
      ellipse(450, 100, 100, 55);
      ellipse(500, 85, 100, 55);
      ellipse(455, 65, 100, 55);
    
      // Flower 1:
      stroke(0, 153, 0);
      strokeWeight(1)
      line(754, 378, 754, 400);
      stroke(0, 153, 0);
      ellipse(756, 390, 4, 1);
      stroke(255);
      strokeWeight(0.5);
      fill(204, 204, 255);
      ellipse(750, 376, 6, 6);
      ellipse(758, 376, 6, 6);
      ellipse(754, 373, 6, 6);
      ellipse(757, 381, 6, 6);
      ellipse(751, 381, 6, 6)
      noStroke();
      fill(255, 255, 112);
      ellipse(754, 378, 5, 5);

      // FLower 2:
      stroke(0, 153, 0);
      strokeWeight(1);
      line(780, 378, 780, 400);
      stroke(0, 153, 0);
      ellipse(782, 391, 4, 1);
      stroke(255);
      strokeWeight(0.5);
      fill(255, 102,102);
      ellipse(780, 370, 7, 7);
      ellipse(776, 374, 7, 7);
      ellipse(784, 374, 7, 7);
      ellipse(777, 379, 7, 7);
      ellipse(783, 379, 7, 7);
      noStroke();
      fill(255, 178, 102);
      ellipse(780, 375, 6, 6);
      
   }

   
   