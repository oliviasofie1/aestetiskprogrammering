### Algorithmic procedures - Flowchart
#### MiniX9
#### Group 4 - Lasse, Signe, Laura & Olivia


#### Individual flowchart:
For this miniX we needed to create a flowchart over one of the most technically complex miniXs I have made. Therefore, I chose my miniX6 about object abstraction, where I created a game. I have decided on this miniX, because it was the assignment, which I found the most technically complex but also the most default. For this reason, it is very useful for me to make a flowchart, because it can help to elaborate what is going on behind the game. As a result, the flowchart also make it easier for people who does not code themself, because it is an easy to understand diagram, where every step is explained in a very simple way.

You can see the result of the flowchart over my MiniX6 - the game "CyberGuard: Digital Vaccination" here:
![](flowchartMiniX6-miniX9.png)


#### Group flowchart:
##### Idea 1:
	• A screen, where there is 4 characters you can pick either Lasse, Laura, Signe or Olivia
	• The player pick the characters they look most like, when the player have picked the game starts
	• A game, where you have to caught water to safe the earth - the player is slower after which player you pick
	• The game ends when the water is not caught and then game changes to screen 3, where a statement will stand
	• The open debate: Safe the earth - climate changes

![](flowchart1.png)

##### Idea 2:
	• Who is most likely to earn the most?- the user picks a character either Lasse, Laura, Signe or Olivia
	• Screen change and you see a movie playing, where you see the work life for the player
	• When the movie is done, then the end screen comes up and then it reads if you are right or wrong.
	• The open debate: Female earn less even though they have the same job

![](flowchart2.png)


#### What are the difficulties involved in trying to keep things simple at communications level whilst maintaining complexity at the algorithmic procedural level?:
There are some different difficulties when trying to make a flowchart that is simple at communication level. We have discussed in the group that we find it way easier to develop a flowchart about a future/imaginary algorithm than creating one a flowchart over an already existing algorithm. On the other hand, when doing a flowchart it can take more time to make, because we have to communicate and decide about all the steps of the program. Therefore you often have to stop and discuss the different functionalities and define how they are specifically going to work. Whereas when you develop the flowchart after programming you have a guideline for the finished result. 

In the book Aesthetic Programming: A Handbook of Software Studies chapter 9 about "Algorithmic procedures'' they mention some elements, which can be challenges to turning an existing program into a flowchart. This includes translating the programming syntax and functions into understandable and more plain language. Moreover, a challenge can be deciding on the level of detail, and deciding which operations shall allow other people to understand the logic of the program. These were also some of the challenges we faced, when doing our flowcharts. 


#### What are the technical challenges facing the two ideas and how are you going to address these?:
In both of our games we want to have different screens that appear throughout the games. It might be a challenge to change in between these screens, and make them appear at the precise time we want. However, we have all tried making small video games, so we can use our experience from the previous exercises when switching in between “stages” in the game in this exercise as well.

Idea 1: 
One of the things we find most challenging looking forward, is making the game different for all of the characters. We want to make different obstacles appear based on the picked characters characteristics - eks. if you pick Lasse, pizzatays should also fall from the sky. 

Idea 2:
In our second idea we want to implement sound, which can be a little tricky sometimes as the different browsers have different rules for e.g. copyright. In chrome sound is only enabled by a click on the webpage. We might be able to address this problem by adding an on-click event where the sound will go off.


#### In which ways are the individual and the group flowcharts you produced useful?:
Our individual flowcharts are relevant in making us understand our own code better and being able to explain the functions to outsiders in a simpler and more understandable way. 

As explained in the book most software applications are mostly not developed only by a single programmer. On the other hand they are made and organized into tasks that are tackled collaboratively by programmers. Therefore, it has been very useful to make the flowcharts together, because we have to code the program together as a group.

By using flowcharts we can effectively formulate ideas, observe relations and generate discussion about the project we want to create. Maybe in the end this can help us to create a better program, where all of us have been heard.


#### Reference:
Soon Winnie & Cox, Geoff, "Algorithmic procedures", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp.213-227.

[Aesthetic programming, “Algorithmic Programming”]( https://aesthetic-programming.net/pages/9-algorithmic-procedures.html)

<br>
[Whimsical](https://www.whimsical.com)
<br>

