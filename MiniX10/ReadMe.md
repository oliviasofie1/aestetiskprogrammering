### "Who will make the most money? - MiniX10 - Final Project
#### Group 4 - Lasse, Signe, Laura & Olivia


[_Click to view our **MiniX10**_](https://oliviasofie1.gitlab.io/aestetiskprogrammering/MiniX10)

[_Click to view our **code: MiniX10.js**_](https://gitlab.com/oliviasofie1/aestetiskprogrammering/-/blob/main/MiniX10/MiniX10.js?ref_type=heads)

#### Screenshots of the program:
Start screen:
![](frontScreen.png)

Working screen:
![](workingScreen.png)

End screen if you picked the women:
![](screenWrong.png)

End screen if you picked Lasse (the man):
![](screenRight.png)

#### Flowchart over the program:
The flowchart, which we made before doing the program can be seen underneath. We had an overall idea for the program before we started with it. These are described here:
- Who is most likely to earn the most?- the user picks a character either Lasse, Laura, Signe or Olivia
- Screen change and you see a movie playing, where you see the work life for the player
- When the movie is done, then the end screen comes up, where the user can see if they are right or wrong.
- The open debate: Women earn less even though they have the same job as men

![](flowchartBefore.png)

#### Flowchart 2.0:
We decided to create a flowchart over the end project, which we made after the project after we got done with it. The flowchart can be seen underneath:

![](flowchartAfter.png)

#### What is your software about (provide a short description of what it is, how it works, and what it sets out to explore)?
The focus of our program is to shed a light on the wage gap between men and women. We want to inform the users of the program about the important issue with men and women not being paid the same - even after doing the same amount of work. 

Our program starts off with screen0: 
This screen portrays every member of our studygroup in front of a factory. Below us buttons appear, encouraging the user to pick a character based on our qualities. 
We have chosen to make the screen resemble a video-game to make it look fun and intriguing. We don’t want the user to know what the intention of the program is before they pick a character. We have tried to make Lasse a less intriguing  character by making him seem more lazy. In that way the users might discover how unfair this inequality can seem. After picking a character the screen will go to screen2

Screen1:
On this screen the same characters appear again but now moving around with boxes. The same boxes run along an assembly line in front of us, to make the user see that we do the exact same amount of work. In the background a happy working song is playing. On this screen the user is not able to interact in order to make the user understand exactly how unfair it seems and how powerless you are in this inequality - you can’t even change anything, Lasse will just earn more money. We wanted to make this working-screen non-interactive so that the watcher of the art piece could not adjust anything in order to make the women earn more than the man - since that is also the case in real life. Some places women can earn more if they make a bigger effort but not at all jobs. In this screen1 we wanted to show the inequality of being hired for the same job, having the same efficiency but still earning more money as a man.
After the song is done playing the screen will move on to the last screen of the program. 

Screen 2/3:
on the last screen the user will discover the purpose of the project. You will be led to different screens depending on whether you chose a male or female character. If you chose Lasse it will say “You are right, Lasse will earn more money because he’s a male.” and if you chose a female it will say “Ups, you forgot that women earn less than males”. On both screens pictures of statistics showing the wage gap will appear. These are here to make it more visible to the user that there is a serious problem. There is a short text of  information on the subject. After this the program is done.

#### How does your work address at least one of the themes and explore the intersections of technical and cultural aspects of code?
A part of our program is created to be more automatic in how it functions. A lot of software nowadays shows autonomy, which we wanted to portray in our code. The visual aspects of our code are typically wrapped in a loop that creates the boxes automatically.

The code also reflects some considerations of societal and gender norms. The initial scenario the user is presented with, where gender can influence the earning potential, which is a cultural reality that exists in multiple societies. This theme inside the code makes it easier to reflect on these issues and perhaps reconsider their own biases. In terms of technical aspects, our code employs simple interactive elements like buttons and conditional logic to create an interactive user experience. These technical aspects go seamlessly with our cultural point, since users are able to interact and explore implications of these choices throughout the art-piece.

The initial screen 0 portrays diverse characters that prompt cultural consideration of representation in a workplace setting. Since these characters are active choices that the user can choose between, the project should implicitly make the user acknowledge the importance of representation. Also including visual elements like images in the code makes them more relatable and easier to engage with. We wanted the project to be multimedia loading pictures which enhances the experience of the project immersing users in a more dynamic experience. By loading the images through the preload (); allows users regardless of internet connection or device capabilities to have access to the multimedia format of our art project. Inclusivity is an important topic in our culture today, therefore we wanted to make sure people can engage with our project while encountering delays and extreme loading issues.

#### Open question: To what extent can the artifact be considered to be critical work in and of itself?
Our program is itself a critical work, where we try to set the focus and start a debate, about equal rights and the gender wage gap, in the workplace, between men and women, which is still a big topic today. We have tried to create a critical and adversarial design, which can help to start a discussion about the difference between men and women having the same jobs but getting paid differently. In this way we want to showcase to the world that everything is still not perfect yet. Even though women and men in many ways are equal, then there are still some problems, which we need to take into account. 

We have decided to pick only one specific focus, women and men, but we could also have looked at race, sexuality, religion and age on workplaces, which could give the artifact more diversity and include more categories into the project. We decided as a group not to do this, because we wanted to set the focus on gender equality, so the purpose of the art piece got shown very clearly for the viewer. In this way, we know that, when picking something then we leave something out, which can also be important, but we have decided that our project only should show one side. 

When thinking about how we can create a critical design, which focuses on something that we as a good find important, we got a lot of inspiration from the project and art piece called:  79 percent clock. This clock was given to people in workplaces, to serve as a daily reminder that at a certain point, the gender wage means women are not being paid for their work. The clock was set to ring after women have been working 79% of the day, which would indicate that the women now work for free compared to their male coworkers. This art piece got made, because studies in the U.S shows that women who work full-time are paid only 79% of what men make annually. They wanted to put the statement, that when a woman hears its chime, then she might as well just go home. This art piece was to enlighten users of gender equality in a more direct way, because when the clock rang then everyone could hear it. For our group, this was a very interesting take on the topic and we were thinking about how we show the same but through coding. In the same way, as the 79 percent clock, we want through our project to make people aware of the gender wage gap. In our mind, we have successfully done that through our program by indicating and showing the user that at the end of our program. 

#### Which MiniXs does the final project fit the best to?
MiniX6 - Object Abstraction -  Object-oriented programming (OOP): 
This project uses the concept of “Object Abstraction” from Chapter 6 of the “Aesthetic Programming: A Handbook of Software Studies” through the creation and utilization of classes like “Box”, that encapsulates data and behavior. The abstraction of the boxes moving on the assembly line, showcases how OOP can be organized and easier manageable. The “Box” class also allows autonomous creation of multiple boxes, so we don't hard-code the boxes’, since it has reusability.  The program itself is very event-driven particularly in handling button clicks as well as screen transitions; it aligns with how OOP encapsulates functionality inside objects. By using OOP it would allow use of methods (behaviors)  and properties from inside the class to manipulate and control where the boxes would move and be shown on the screen itself. Some of the qualities of OOP ensure easier maintainability and scalability in the program, since much of the functions are inside the class “Box” making the code more human-readable.


MiniX2 - Fun with Geometry -  in the aesthetic part:
Taking a look at the aesthetic in our software art piece it corresponds a lot to the MiniX2 we did about how emojis fit into a wider social and cultural context. When taking this into consideration one must think about how the product will concern politics of representation, identity, race, colonialism, etc. Just by looking at the situation with the “new” emojis (where one was able to change the skin color of the emoji) it was clear how the society was affected by the way identity was represented. Responses of the new feature did not just bring happiness about more identity representation but showed that identity is not just about skin color. That is also why politics of representation and identity can be so complex.

However, as mentioned before, we wanted to represent equality in work areas with our game, so we must consider the complexity of these politics of representation. How could we reconfigure the remainder af DiSalvio would say. The whole idea of Adversarial design is the concept of reconfiguring the remainder where one designs a good solution for the minority. It is a different way of creating a debate about something instead of just talking about it. Our project is another product in the series of work-equality but with a more artsy take on it.

When focussing on gender one must also consider what is getting into the project and what is left behind. Twisting the focus of our final project one could easily have done a man-representing project about how hard-working men are or one could also represent the power the boss has at a workspace. Our project was probably also a bit inspired by the fact that three out of four from our study group are women. Therefore we thought we had a good warranty for representing women. However when we chose to have the focus of work-equality we also did our research on how the equality actually took place. Our art piece is therefore made by three women but the representation of gender is still based on real study.

Overall we wanted our code to be a representation of a part of the society. We like the idea of being able to do that through a piece of software. The benefit of creating an adversarial design is that you don’t have to consider the other part of the product. In our case we did not have to think about the consequences for men. That gave us the chance to represent how unfair this equality actually is and put into the light that something must be done about it.

#### Reference:
[79-work-clock] (https://whatever.co/work/79-work-clock/)
<br>
[Aesthetic programming book] (https://aesthetic-programming.net/)
<br>

<br>
ChatGPT




