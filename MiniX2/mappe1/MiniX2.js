//Declaring global variables:
let img;
let button1Clicked = false; //Variable to track whether button1 is clicked
let circleColor = 'white'; //Color of the emoji, before interaction.
let button2Clicked = false; //Variable to track whether button2 is clicked
let button3Clicked = false; //Variable to track whether button3 is clicked
let button4Clicked = false; //Varibale to track whether button4 is clicked
let button5Clicked = false; //Variable to track whether button5 is clicked
let button6Clicked = false; //Variable to track whether button6 is clicked
let button7Clicked = false; //Variable to track whether button7 is clicked

//loading image - preload makes sure it runs before the rest of the code.
function preload(){
  //Load the image in the preload function
  img = loadImage('reminder1.png');
}

//runs once in the start of the program - good for fixed settings
function setup(){
  createCanvas(windowWidth, windowHeight);

   // Create a button: The done button:
   let button1 = createButton ('Click here when I am done')
   button1.position(410, 580);

  //Use the button to load the image
  button1.mousePressed(() => {
    // set button1Clicked to true when the button is clicked 
    button1Clicked = true;

  // Set a timeout to reset buttonClicked after 8 seconds
    setTimeout(() => {
      button1Clicked = false;
    }, 8000);

  });

  //Create buttons on the phone:
  //Button for feeling angry:
  let button2 = createButton('Angry')
  button2.position(838, 500);
  //add style to the element by adding a CSS declaration
    //the key-value pair of a CSS property ('color') and its value ('red')
  button2.style("font-family", "NORMAL");
  button2.style('font-size', '15px');
  button2.style('color', 'red' );
  button2.size(55, 25);
  button2.mousePressed(()=>{
    // Set circleColor to red when button2 is clicked
    circleColor = 'red';
  });

  //Button for feeling happy:
  let button3 = createButton('Happy')
  button3.position(898, 500);
  button3.style("font-family", "NORMAL");
  button3.style('font-size', '15px');
  button3.style('color', 'lightpink');
  button3.size(55, 25);
  button3.mousePressed(() => {
    // Assigning the variable "circleColor" to lightpink when button3 is clicked
    circleColor = 'lightpink';
  });


  //Button for feeling alright:
  let button4 = createButton('Alright')
  button4.position(960, 500);
  button4.style("font-family", "NORMAL");
  button4.style('font-size', '15px');
  button4.style('color', 'green');
  button4.size(55, 25);
  button4.mousePressed(() => {
    // Set circleColor to yellow when button4 is clicked
    circleColor = 'green';
  });

  //Button for angry mouth:
  let button5 = createButton('Nr.1')
  button5.position(838, 545);
  button5.style("font-family", "NORMAL");
  button5.style('font-size', '15px');
  button5.size(55, 25);
  button5.mousePressed(() => {
    // Set button5Clicked to true and other mouth buttons to false
    button5Clicked = true;
    button6Clicked = false;
    button7Clicked = false;
  });

  //Button for happy mouth:
  let button6 = createButton('Nr.2')
  button6.position(898, 545);
  button6.style("font-family", "NORMAL");
  button6.style('font-size', '15px');
  button6.size(55, 25);
  button6.mousePressed(() => {
    // Set button6Clicked to true and other mouth buttons to false
    button6Clicked = true;
    button5Clicked = false;
    button7Clicked = false;
  });

   //Button for happy mouth:
   let button7 = createButton('Nr.3')
   button7.position(960, 545);
   button7.style("font-family", "NORMAL");
   button7.style('font-size', '15px');
   button7.size(55, 25);
   button7.mousePressed(() => {
    // Set button7Clicked to true and other mouth buttons to false
    button7Clicked = true;
    button5Clicked = false;
    button6Clicked = false;
  }); 
  
  
}

//draw loops through its scope 60 times pr. second 
function draw(){
  background(173, 216, 230);

  // Setting the cursor to hand: 
  cursor(HAND);

  // Headline:
      textSize(65);
      fill(255);
      stroke(0);
      strokeWeight(5);
      textStyle(BOLDITALIC);
      text('EMOJI MAKER', 450, 100);

  //Arm under the phone:
    fill(204, 153, 255);
    stroke(255);
    strokeWeight(4);
    rect(835, 485, 145, 2000);//arm
    fill(204, 153, 255);
    stroke(255);
    strokeWeight(4);
    rect(765, 595, 275, 100, 65, 55, 65, 55);//hand
    arc(790, 628, 45, 350, PI, TWO_PI);//thumb //An arc is a section of an ellipse defined by the x, y, w, and h parameters.

  // Phone and screen where the emoji is created:
  //Phone:
    fill(100);
    stroke(1);
    strokeWeight(2);
    rect(800, 240, 260, 400, 15, 15, 15, 15); 
    fill(255, 150);
    noStroke();
    rect(910, 255, 50, 6, 20); 
    ellipse(885, 258, 11, 11);
    ellipse(860, 258, 11, 11);
    ellipse(975, 258, 6, 6);
    fill(255);
    noStroke();
    rect(1061, 325, 4, 50, 6);
    rect(1061, 400, 4, 25, 6);
  //Screen:
    fill(200);
    stroke(1);
    rect(815, 285, 230, 330, 15, 15, 15, 15);
  //Text inside:
    let s="CREATE ME" //creating a variable which holds a string (text), which gets displayed on the phone
    stroke(1);
    textSize(17);
    fill(255);
    text(s, 876, 315);
    

  //Fingers and fingernails on the phone:
    fill(100);
    stroke(255);
    strokeWeight(2);
    fill(204, 153, 255);
    rect(1035, 450, 40, 40, 55, 10, 10, 55);//finger upper
    rect(1025, 500, 50, 40, 55, 10, 10, 55);//finger middle 
    rect(1015, 550, 60, 40, 55, 10, 10, 55);//finger lower
    fill(255, 205, 229);
    strokeWeight(2);
    rect(1034, 460, 28, 22, 55, 5, 5, 55);//fingernail upper
    rect(1026, 510, 28, 22, 55, 5, 5, 55);//fingernail middle
    rect(1018, 555, 30, 27, 55, 5, 5, 55);//fingernail lower
    arc(782, 500, 11, 85, PI, TWO_PI);//fingernail thumb //An arc is a section of an ellipse


  //Textbox under the phone:
    fill(255);
    noStroke();
    rect(1110, 600, 180, 100);
    triangle(1080, 615, 1110, 640, 1110, 615);
    //Text inside the box:
    let p="Let's Get It Started"
    stroke(255);
    textSize(15);
    fill(random(100, 250),random(100, 250),random(100, 250));
    text(p, 1135, 625);
    //Under text inside the box:
    let p1="To Start: Click on the buttons"
    stroke(255);
    textSize(12);
    fill(140, 220, 255);
    text(p1, 1118, 660);


  // Textbox with information:
    fill(235);
    stroke(1);
    rect(270, 240, 325, 400, 15, 15, 15, 15);
  //Text inside:
    let h="Information"
    stroke(1);
    textSize(25);
    fill(173, 216, 230);
    text(h, 362, 280);

  //Little textbox 1:
    fill(0, 106, 255);
    stroke(0, 125, 255);
    rect(295, 325, 150, 50, 20);
  //Text inside the little textbox 1:
    let t0="Hi! I'm Emoji."
    noStroke();
    textSize(12);
    textStyle(NORMAL);
    fill(255);
    text(t0, 320, 355);

  //Little textbox 2:
    fill(0, 106, 255);
    stroke(0, 125, 255);
    rect(295, 390, 268, 50, 20);
  //Text inside the little textbox 2:
    let t1="Create me and make me feel like you do today."
    noStroke();
    textSize(12);
    textStyle(NORMAL);
    fill(255);
    text(t1, 306, 420);

  //Little textbox 3: 
    fill(0, 106, 255);
    stroke(0, 125, 255);
    rect(295, 460, 265, 67, 20);
  //Text inside the little textbox 3:
    let t2="First pick: The color you feel like today."
    let t3="Next pick: A mouth that fits, how you feel."
    noStroke();
    textSize(12);
    textStyle(NORMAL);
    fill(255);
    text(t2, 315, 490);
    text(t3, 315, 508);


  //Creating the start emoji:
    fill(circleColor);
    circle(930, 400, 140);

  //Eyes:
    fill(255);
    ellipse(908, 385, 25, 40);  
    ellipse(950, 385, 25, 40);
  //Inside the eye left 
    fill(0);
    stroke(255, 20);
    strokeWeight(2);
    ellipse(908, 390, 15, 18);
    fill(255);
    ellipse(910, 395, 6, 7);
  //Inside the eye right
    fill(0);
    stroke(255, 20);
    strokeWeight(2);
    ellipse(951, 390, 15, 18);
    fill(255);
    ellipse(953, 395, 6, 7);
  
    //Eyebrows - making the left eyebrow raise if mouse is pressed
    if (mouseIsPressed) { // raises the left eyebrow
      noFill();
      stroke(0);
      strokeWeight(4);
      curve(878, 378, 898, 348, 918, 348, 938, 378); // left eyebrow
      curve(920, 378, 940, 358, 960, 358, 980, 378); // right eyebrow
    } else {// standard position of the brows
      noFill();
      stroke(0);
      strokeWeight(4);
      curve(878, 378, 898, 358, 918, 358, 938, 378); // left eyebrow
      curve(920, 378, 940, 358, 960, 358, 980, 378); // right eyebrow
    }


  // Creating the emoji with different mouths based. Making if-statemants to check which button is clicked on
    //the buttonsClicked are global variable with value false, which changes to true if the button is clicked
    if (button5Clicked) { // Angry mouth
    fill(0);
    rect(902, 435, 55, 5);
    } else if (button6Clicked) { // Happy mouth
    fill(0);
    arc(930, 430, 50, 50, 0, radians(180), PIE);
    } else if (button7Clicked) { // Alright mouth
    fill(0);
    arc(930, 435, 65, 11, 0, radians(180), PIE);
    }

 // Drawing the image only when the button is clicked
  // using if statement to check two conditions (true or false):
    // "button1Clicked" = this variable check if button have been clicked (true or false)
    // the function image will be execute if the condition is true. The function will display the "img" = this variable holds the image (load in preload)

    if (button1Clicked && img) {
      image(img, 0, 0, width, height);
    }

 
}



 
