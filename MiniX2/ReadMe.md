## MiniX2: Variable Geometry:

[_Click to view my **miniX2 EmojiMaker**_](https://oliviasofie1.gitlab.io/aestetiskprogrammering/MiniX2/mappe1/index.html)

[_Click to view my **miniX2 ColorMoodScale**_](https://oliviasofie1.gitlab.io/aestetiskprogrammering/MiniX2/mappe2/index.html)

[_Click to view my my **code**_](https://gitlab.com/oliviasofie1/aestetiskprogrammering/-/blob/main/MiniX2/mappe1/MiniX2.js?ref_type=heads)




### **My program and what I have used and learnt:**
In my MiniX2 I have decided to focus on human emotions and feelings, which my emojis displays. I choose to do this, because I wanted emojis, that everyone can relate to and not only some groups of people. 

The program called EmojiMaker is a program that allows users to create and customize an emoji based on their current mood. The EmojiMaker consist of an information screen, where the user step by step, can figure out, how they can create the emoji and make it feel like they do. In addition to that, I decided to make a phone, where the interaction happens. Here buttons are created, so the user can select their current mood and choose the mouth shapes for the emoji, that express their mood further. Other than that, I also decided to create a button, which should be click on after the interaction and emoji are done. Here the user is getting a reminder. I decided on that, because during the process of making the emoji I figured, that it is important to state, that this emoji is not meant to look like the user, but just a digital symbol to represent their emotions and feelings.

I also decided to make a program called ColorMoodScale, where I got a lot of inspiration from the famous "Mood Ring", which I had when I was a small child. I decided on making, as a support to the other program "EmojiMaker". Here I wanted to make an emoji, with all the emotions moods. This I also wanted to do with my other program, but because of lack of time, I decided to make this program and emoji as well. This program also consists of an information screen and a phone, where the user can click and pick the emotion of the day, when mouseIsPressed on the emoji. Here I decided to make the emoji very simple, so only the color can be changed. I decided to create this second program,  because I wanted to have all emotions included in my MiniX. At the same time, this could also show, how the program can expand, but also show how I would like the "EmojiMaker" to look if I had more time on my hand, which would be to have all emotions represented.

You can see the result of my EmojiMaker and the ColorMoodScale Emoji below:
![](EMOJIMAKER.PNG)

![](ColorMoodScale.png)

### **How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?:**
The following week's MiniX2 assignment focused on creating emojis that take into account a wider angle the social and cultural context, recognizing that negative gender portrayals and racial discrimination persist across various devices and operating systems. 

To take this into account, my two programs and emojis are created with inspiration from "Modifying the Universal". Some key points, that I took away from reading the text was that; Emojis started off as away to express our emotions through text messages, but that have changed. Now it has become a way to identify yourself, whether that's skin tone or gender expression. Today it's hard to represent everyone, because you can never truly create diversity that represents every single user through emojis. Emojis are extremely limited no matter how many you decide to add to the collection. In this way, there are a lot of people who feel not represented, which make users question the color of emojis, because they can't identify themself with the emojis. This is a big problem in the social and cultural context, and several petitions ask to get it solved, because there need to be more diversity in emojis.

Because of this, I decided, in my two programs and emojis, to focus on feelings and emotions, because I wanted to go back to what emojis started out as, which was a way to show how we feel inside. I wanted to focus on this, because I wanted to avoid differentiation between different cultures and ethnicities. At  the same time, I also wanted to make everyone to feel welcome, because the emojis are "just" a way to display your emotions and not your looks and cultural background. 

My emojis are created to be very simple, because I wanted it to be a way to show "real" emotions. Therefore I picked both in of my programs to take inspiration form the famous "Mood Ring Colorscale". I decided on this, because the user can pick their color of emotion of the day. Other then that, in the EmojiMaker program, I also decided focus on the mouth of the emoji, because it is also an element, which can express emotions. 

In all I picked to focus on emotions, because I think this is an easier way to represent everyone and I wanted to take the emoji back to its original purpose, which was to use them for fun and to display and show human emotions in image form. I think that maybe this could be a solution to solved the issue and make more people feel represented in the social and cultural context.

### **Reference of what I used::**
- https://p5js.org/reference/#/p5/let
- https://p5js.org/reference/#/p5/fill
- https://p5js.org/reference/#/p5/strokeWeight
- https://p5js.org/reference/#/p5/stroke
- https://p5js.org/reference/#/p5/if-else
- https://p5js.org/reference/#/p5/text
- https://p5js.org/reference/#/p5/background
- https://p5js.org/reference/#/p5/noStroke
- https://p5js.org/reference/#/p5/ellipse
- https://p5js.org/reference/#/p5/circle
- https://p5js.org/reference/#/p5/line
- https://p5js.org/reference/#/p5/rect
- https://p5js.org/reference/#/p5/triangle
- https://p5js.org/reference/#/p5/textSize
- https://p5js.org/reference/#/p5/preload
- https://p5js.org/reference/#/p5/image
- https://p5js.org/reference/#/p5/cursor
- https://p5js.org/reference/#/p5/textStyle
- https://p5js.org/reference/#/p5/createButton
- https://p5js.org/reference/#/p5.Element/mousePressed
- https://p5js.org/reference/#/p5/arc
- https://p5js.org/reference/#/p5/curve

### **Reference:**
Roel Roscam Abbing, Peggy Pierrot and Femke Snelting. (2017): Modifying the Universal. Executing.  Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press

Soon, Winnie & Cox, Geoff. (2020): Aesthetic Programming: A Handbook of Software Studies. London: Open Humanities Press


