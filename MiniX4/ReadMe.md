### "Thank you for accepting"
#### MiniX4 - Data Capture

[_Click to view my **MiniX4**_](https://oliviasofie1.gitlab.io/aestetiskprogrammering/MiniX4/)

[_Click to view my my **code**_](https://gitlab.com/oliviasofie1/aestetiskprogrammering/-/blob/main/MiniX4/MiniX4.js)

#### My program:
The design "Thank you for accepting" is an attempt on a critical design piece. The program is meant to address the thoughts on the need of capturing data on everything we do in the digital world, which is also expressed in Aesthetic Programming in chapter 4. In the chapter Soon and Cox explains, that we are in the era of big data, where there is a need of capturing data of everything, even the just a simple actions like if a button is pressed. 

You can see the result of my project here:

![](miniX4_image1.png)

![](miniX4_image2.png)

I have created a desktop inspired by MacOS 8.1 User Interface powered by Apple Platinum. I decide on making the old looking interface, because I wanted to show how much technology has developed over the past years. In the same time, I also wanted to connect new with old technologies. For us it might seem old, but in reality many of the things for then we are still using today. I also meant to showcase, how data constantly gets captured from us all, when being digital. I wanted to take us back to the past and put it together with the future, as if a person back then should see how the digital world is today, because back then technology seemed kind of harmless and innocent, and I don’t think many of us could have foreseen how it would evolve.


I created a videoInput variable using the createCapture() function provided by p5.js and face tracker that displays only the face captured by the web camera. The videoInput.hide() function is used to hide the video element on the screen. This is often done to use the video feed for processing without displaying it directly. For the facetracker I downloaded clmtracker provided by the clmtrackr library, which can do face tracking in real-time. Once initialized and started, the faceTracker continuously analyzes the video feed for faces. It detects the position of facial landmarks such as eyes, nose, mouth, etc. The getCurrentPosition() method is used to retrieve the positions of detected facial landmarks in the form of an array of [x, y] coordinates. These coordinates are then used to draw elements on the canvas, here by the text (textOfTheFace) at each detected landmark position. Other then this, I used function preload to load all of the small icons for the desktop. I also created my own function for the information window with the x and y positions as variables to enable me to create multiple windows that I could stack on top of each other. I also included one DOM element button, which I styled with CSS, and I added the eventlistener mousePressed to the button, so if the button is pressed then then an infomation window will be drawn on the canvas.


I created information windows with only one button, which is an accept button, which the user needs to click on. When you click on accept, there will come up a new info window up, which explains, that now we can achieve and capture even more data about you. The accept buttons are created and meant to remind you a little bit of the “accept cookies” button on websites, where you in reality do not really have a choice, it is more of an information. I wanted to make only the accept button, because no matter have many times you say no, then we always end up accept something. I also wanted to emphasize the problem, which is that we are fast at accept things, for instead cookies, without thing and reflection upon the consequences it can have. We never really read everything, because we are to lazy or maybe we just want things to go fast, because we don't want to use time on removing all the cookies. Which means, that our data can be captured, and everything we do digital can be analyzed without any problems. I find that kind of scary, because do all of us even understand that, this is happening??

The mean element for me was also to illustrate, that no matter how much we try to distance us from it, somehow there will always be capture something from us, which the face tracker is showing in the project. It shows, that you can't just move away, because in a way you will always be seen, because you can't be totally neutral online.


#### What are the cultural implications of data capture?:
In the creation of my program, I got a lot of inspiration from the VPRO documentary about Surveillance Capitalism. The cultural implications of data capture, as Shoshana Zuboff discusses encompass a wide range of issues that deeply affect our society. Surveillance capitalism profoundly shapes culture in several ways. 

First, it undermines traditional privacy norms by tracking and analyzing personal data. Even more of, this is analyzed to train models, which analyses patterns of human behavior, when they have the behavioral surplus the comprehensive  behavioral data of hundreds of millions  of people, then they can start predicting the preferences of specific groups of people, which is crazy to think about. One of the visualization tools to do user analysis is heatmaps, which provides a graphical representation of data to visualize user behavior. This is specifically useful for marketing purposes and to understand which content is more or less attractive to users. In the same way it's also used by companies or political parties to analyze where to best place their ads and other propaganda. 

Second, the use of algorithms fueled by captured data enables unprecedented manipulation and influence over individuals. This extends beyond online interactions to influence peoples real-world behaviors, because they can create adds, that can influence all from political beliefs, who they should hang out with to purchasing decisions. 

But as Professor Shoshana Zuboff describes, there is still a lot that digital brings to our daily lives, and we deserve, as humans, to have all of that, without paying the price of surveillance capitalism. As citizens we should not have to make a choice of either going totally analog to get away from all of this, or living in a world, where our privacy and self-determination are destroyed for the sake of this market logic just because companies, what profit from all of this. I think she is right, I also find it a bit creepy to think about, that just from one click or conversation companies can capture so much data about you. So yes, I think we need to do something about this. These challenges need to be addressed, to protect individual rights and ensure accountability in the use of personal data. I don't think it's easy, and as Zuboff say, navigating this complex landscape demands ongoing adaptation to the evolving dynamics of surveillance capitalism. But how do we do that???

### Reference:
Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119

VPRO Documentary: Shoshana Zuboff on surveillance capitalism | VPRO Documentary 

Mac OS 8: https://upload.wikimedia.org/wikipedia/en/5/59/Mac_OS_8.1_emulated_inside_of_SheepShaver.png 

https://p5js.org/reference/