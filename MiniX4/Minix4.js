let positions; //array with [x,y positions]
let textOfTheFace="FACE";
let buttonX;
let acceptButton;
let showBigBoxWithText = false; // Variable to track the visibility of the showBigBoxWithText
let menuIcon1;
let menuIcon2;
let trashIcon;
let computerInfoIcon;


function preload(){
  menuIcon1= loadImage('menu.png');
  menuIcon2 = loadImage('menu1.png');
  trashIcon = loadImage('trash1.png');
  computerInfoIcon = loadImage('computerMenuIcon.png');
}

function setup() {
  createCanvas(650, 500);

  // setup camera capture
  let videoInput = createCapture(); //variable assigned with function provided by p5.js (starting web camera)
  videoInput.size(600, 500);
  videoInput.position(0, 0);
  videoInput.hide(); //hide video element on the screen

  // setup the facetracker:
  faceTracker = new clm.tracker(); //initializes the face tracking object (clm library - real time face tracking)
  faceTracker.init(pModel); //method init initializes the tracker with pre-trained model "pModel"
  faceTracker.start(videoInput.elt); //method "start" begins the face tracking process //videoInput.elt HTML element corresponding to video feed

  textOfTheFace=textOfTheFace.split(''); //Splits "FACE" into pieces and returns an array containing the pieces ["F", "A", "C", "E"] 


  //creating a DOM element button and styling the button with CSS
   acceptButton = createButton('Accept');
   acceptButton.position(480, 305);
   acceptButton.style("padding","4px"); //spacing inside the button
   acceptButton.style("color","black");
   acceptButton.style("outline","none");
   acceptButton.mousePressed(() => { //adding the eventlistener to acceptButton using mousePressed() function.
    showBigBoxWithText = true; // Set showBigBoxWithText to true when button is pressed. Controls if the bigbox should be drawn on the canvas or not. 
  });


  }

  let j=0; 

function draw() {
  // Set the background color here:
  background(171, 173, 255);  

  // positions gets the currenct position [x, y] of landmarks from the face tracker.
  positions = faceTracker.getCurrentPosition();


  //loops over each landmarkers position stored in the position array
    //each characters from "FACE" gets an position
  for (let i=0; i < positions.length; i++) { //i < positions.length ensures that there is element left in the array.
    stroke(0);
    fill(255, 0, 0);
    text(textOfTheFace[j],positions[i][0],positions[i][1]); //takes a character of the array "textOfTheFace" and provide it with an x and y
    j++; //incremented (++) to move to the text character in the array "FACE" array
    if(j>=text.length){ //if j is bigger or equal to the length then "FACE"
        j=0; //reset to j to 0 ensuring the text cycle trough the array again
    }
  }

//info windows
  box(440,140);
  box(450,150);
  box(460,160);

  box(470,170);
  box(460,180);
  box(450,190);
  box(440,200);
  box(430,210);

   //the icons and their positions and size:
   image(menuIcon1, 0, 476);
   menuIcon1.resize(350, 23);
 
   image(trashIcon, 585, 440);
   trashIcon.resize(50, 0);
 
   image(menuIcon2, 0, 0);
   menuIcon2.resize(650, 18);

   image(computerInfoIcon, 8, 18);
   computerInfoIcon.resize(140, 0);
  

  //the information window
  function box(x,y) {
    //large rectangle
    fill(191);
    rect(x,y,150,130);
  
  //rectangle header
  fill(105);
  rect(x+3,y+3,143,20);
  
   //crossbutton inside a little box:
   fill(191);
   stroke(0);
   rect(x+128,y+6,14,14);
   fill(0);
   textSize(12);
   text('x', x+132,y+17);
   fill(0);
   textSize(12);
   text('x', x+132,y+17);
 
  //text inside the boxes:
  textSize(10);
  text('Hi, there you are. I See You!!', x+10,y+65);
  text('Please, click accept...', x+10, y+85);
  }

   // Draw the big box with text only when showBigBoxWithText is true
   if (showBigBoxWithText) {
    drawBigBoxWithText();
  }

// Calling the drawBigBoxWithText function if showBigBoxWithText is true. Function to draw the big box with text:
function drawBigBoxWithText() {
  // Large rectangle
  fill(191);
  rect(100, 100, 250, 200);
  //rectangle header
  fill(105);
  rect(105, 105, 240, 20);
  //crossbutton inside a little box:
  fill(191);
  stroke(0);
  rect(327, 108, 14, 14);
  fill(0);
  textSize(12);
  text('x', 331, 119);

  // Text inside the big box
  fill(0);
  textSize(14);
  text("Thank you for accepting.", 110, 180);
  text("Now we can capture everything.", 110, 205);

}

  }






