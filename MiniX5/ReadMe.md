### A generative program
#### MiniX5 - Auto-generator

[_Click to view my **MiniX5**_](https://oliviasofie1.gitlab.io/aestetiskprogrammering/MiniX5/)

[_Click to view my my **code**_](https://gitlab.com/oliviasofie1/aestetiskprogrammering/-/blob/main/MiniX5/MiniX5.js)


#### What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?:
For this weeks MiniX, the theme has been "Auto-Generator". I decided to make an a more abstract and different program, where I have been using shapes and colors, which should create a  different and new expression the longer the program runs. In my program I have made some four simple rules to create a foundation for the auto-generated program. The rules are declared blow:

	1. Create a circle, which is moving from the top of the corner (x=0, y=0)
	2. When the circle hits the end or the corner of the window it will push back in a 90 degree angle
	3. When the circle hits the x-axis to the left or the right, then the circle will change to a random color.
	4. When the circle hist the y-axis to either the left of the right, then the circle will change to a random color. 

As explained before, I created these rules, so it could generate a fascinating pattern the more the program runs. For the start of the program I decided on creating a kind of blank pages. I decided on creating a grid background, which is created with a for-loop,/nested loop. I made this to make the program appear more interesting and also add another type of shape to the program. Other then this, the grid background can be seen as a visual representation of the underlying algorithms or rules governing the auto-generation process. In the same time, it serves as a background against the auto-generated element, which is the moving circle.

I used If-statements in my program to check the position of the circle. The if-statements control the behavior of the circle, when it reaches each boundary, which is the right side, left side, bottom and top boundaries. If the circle reaches one of boundaries of the canvas, which means that the conditions is true, then it ensures that it bounces off the edges and changes the color to a random color.

The moving circle is creating a kind of line, when moving, which creates different shapes such as triangles, rectangles and half circles as it runs. The browsers windows size can have an effect on the shapes and how the program runs, which means that there can be generate different shapes, lines and forms. Therefore you can play around with the window size to see, how the program changes and looks different.

You can see the result of the program here:
![](miniX5-picture1.png)

![](miniX5-picture2.png)

![](miniX5-picture3.png)

#### What role do rules and processes have in your work?
In the start, I was thinking about, what kind of rules, that I could create, but I know that I wanted to make something move and make if-statements to make it changes direction. In the same time, I decided to make something simple, but after working with the code and looking at the programming running I found out, that simple rules can create something very interesting and abstract. 

After working and creating the rules and the program, I wanted to play around with different colors. I wanted to make the color change randomly if the circle hit a corner or the end of the canvas. Therefore I decided to use the function random(), because it can best illustrate the different shapes and forms, that get created when the circle moves around. This is also making a very unpredictable pattern, which can attract attention.

In total, the rules tells the program how, circle should move and change direction in a 90 degree angle, when it hits the end or the corner of the window In the same time. More then that, the rules tells how the color should change a random color (see js file), depending on if it hits the x- or the y-axis.



#### Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?
Drawing upon the assigned reading, the MiniX program, that I made, exemplifies the idea of an auto-generator by showcasing how simple rules can lead to interesting and unpredictable outcomes. This program demonstrates levels of control and autonomy through the implementation of specific rules governing the behavior of the moving circle. These rules command the movement, direction changes, and the color transitions of the circle, providing a structured environment in which the generative process evolves. 

More then that, my program highlights the concepts autonomy by allowing the generative process to operate independently once the rules are established. When the program is initiated, the circle then moves according to the predetermined rules, creating patterns and shapes on the canvas without direct intervention. This independence highlights how computational systems can demonstrate self-guided actions while operating within preset boundaries.

Additionally, this MiniX program illustrates the importance of exploring the boundaries of randomness, chance operations, and computational creativity. By trying out and exploring new ideas within the rules of the program, the programmer can discover new ways to make art and explore technology. Furthermore, this chapter delves into the concept of auto-generation, which highlights the role of rules and autonomy in creative computational systems. It prompts reflection on the evolving relationship between humans and machines in the creative process and how automation influences societal dynamics. By exploring these themes, the chapter encourages deeper engagement with the potential of computational creativity and its implications for innovation and societal change.

#### Reference:
Soon Winnie & Cox, Geoff, "Auto, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142

Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10. Cambridge, MA: MIT Press, 2012, pp. 119-146. 