let circle1; //declare the variable rectangle, that will be used later

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(200);

  // creating a nested-loop make the gridder background:
    //This initializes a nested loop where i (rows) and j (colums) starts from 0 and continues until it reaches the width of the window and height
  for(let i = 0; i < windowWidth; i++) { //iterates the width (i)
    rect(0+i, 0, 10, 10) 
      i += 10
    for (let l = 0; l < windowHeight; l++) { //iterates the height(j)
      rect(-10+i, 11+l, 10, 10) //size - 10x10 pixels
      l += 10 
      }
    }

  //Instantiate a new object for my circle class:
    circle1 = new Circle(0, 0); //starting at 0,0 - top-left corner

   }
   

   function draw() {

    //IF the cirlce hits the right side of the screen:
    if (circle1.x > windowWidth) {
      circle1.xdirection = circle1.xdirection * (-1);
      circle1.color = color(random(200, 255), random(150, 255), random(100, 255));
       }
     
    //IF the circle hits the bottom of the screen:
    if (circle1.y > windowHeight){
      circle1.ydirection = circle1.ydirection * (-1);
      circle1.color = color(random(200, 255), random(150, 255), random(100, 255));
    }

    //IF the circle hits the left side of the screen:
    if (circle1.x < 0) {
      circle1.xdirection = circle1.xdirection *(-1);
      circle1.color = color(random(200, 255), random(150, 255), random(100, 255));
      }

    //IF the circle hits the top of the screen:
    if (circle1.y < 0) {
      circle1.ydirection = circle1.ydirection * (-1);
      circle1.color = color(random(200, 255), random(150, 255), random(100, 255));
    }

    //Making the circle move on the canvas:
      //circle1.display() is responsible for rendering the circle on the canvas with the specified properties/attributes.
    circle1.move();
    circle1.display();

  }

  //class Circle holding properties and methods of object circle
    function Circle(x, y) {
      this.x = x; 
      this.y = y;

      //For every time it moves / the circle will move 3 pixels in the x and y direction
      this.xdirection = 3;
      this.ydirection = 3; 
      //setting the starting color:
      this.color = color(random(200, 255), random(150, 255), random(100, 255));
    
    //defines the display method for the Circle object:
    this.display = function(){
      noFill();
      stroke(this.color);
      strokeWeight(1);
      //This function draws a circle at the specified x and y coordinates with a given diameter (400 pixels in this case).
      circle(this.x, this.y, 400);
    }
    //defines the move method of the circle object - updates the circle's position based on its current direction:
    this.move = function(){
      this.x = this.x + this.xdirection; 
      this.y = this.y + this.ydirection;
    }
  }

    
   
   