### Working with APIs - "Find your spirit-Pokémon"
#### MiniX8
#### Group 4 - Lasse, Signe, Laura & Olivia

Please load 2 times if all images are not appearing:)))

[_Click to view our **MiniX8**_](https://oliviasofie1.gitlab.io/aestetiskprogrammering/MiniX8)

[_Click to view our **code: MiniX8.js**_](https://gitlab.com/oliviasofie1/aestetiskprogrammering/-/blob/main/MiniX8/MiniX8.js?ref_type=heads)


#### What is the program about? Which API have we used and why?:
For this week's miniX8 our group came up with the idea that we wanted to create a program called "Find your spirit-Pokémon". We have created this program, because we wanted to find out which Pokémon fits different people. We wanted to connect people and Pokémon's by matching peoples zodiac signs and the description of and facts about the different Pokémon's. In this way, we could easily find out which Pokémon and people fitted best together. 
 
When starting the program, the user needs to click on their zodiac sign, which then shows them what Pokémon fits them. Here the user can see a Pokémon card full of information about how their zodiac sign fits together with the specific Pokémon. Other than this, there can be some central facts about the Pokémon, such as its name, the type and its stats. To get this information about the Pokémon we used an RESTful API, which is a pokéAPI, which returns a JSON-file.

You can see the result of “Find your spirit-Pokémon” here:
![](findyourPokémon1.png)

![](findyourPokémon2.png)

#### Describe and reflect on your process of making this miniX in terms of acquiring, processing, using and representing data?:
Our first idea for this miniX was actually to make a program with a sort of music API. However we found it difficult to really obtain the data we wanted. Therefore we chose a public API on the pokéAPI that Lasse, from the group, already had access to. This made the process of acquiring the data really easy - especially also because it was public so we did not have to gain access to some kind of key in order to use it.

The processing of the data took some time since we had to understand what information the API actually was giving us. However the PokéAPI website was somewhat easy to understand and we got information about each pokemon, their stats, types, HP,  etc. In this way, we were able to gain information about the poke-cards, but also know which data we were able to put on our website.

Representing the data was without doubt the longest process. We brainstormed good ideas for representing the Pokemons in the best possible way. Here we came up with the idea of getting a more ‘personal’ relationship to a Pokemon - through zodiac signs. In this way we tried to reach both the group of pokemon fans but also just people interested to know more about the pokemon world. Therefore we chose to represent the information from the API in one box - which probably was most understandable for the fans and then some extra information on a poke-card on the left for the relationship to the pokemon.

#### How do platform providers sort the data and give you the selected data? What are the power-relations in the chosen APIs? What is the significance of APIs in digital culture?
In this MiniX we have experienced, by ourselves, that the distribution of data through an API is controlled by the provider of the API. The API we used was for free and we did not need to identify ourselves to get and use it, but in other cases you have to identify yourself through an API key facilitator. In other times, the API also costs money to get a hold of. 

In Aesthetic Programming: A Handbook of Software Studies chapter 8 about "Que(e)ry data", they mention Google as an example of a dominating provider. Here they explain that Google and its operations provides its API for experimentation, but it only does so under restrictions. These restrictions are that they are limiting requests for public and non-profit/educational use, and by only revealing some of the available parameters, the logic of how the search data (knowledge) is presented algorithmically is still unknown for the public. This can raise some serious questions about the degree of openness and accessibility of API practices, because we do not have access to all the data Google is holding. In this way, an API can never really be “owned”, and we can never really have ownership of data unless we provide it ourselves.


#### What would we want to know more about? Try to formulate a question in relation to web APIs or querying/parsing processes that you want to investigate further if you have more time.
If we had more time it would be interesting to investigate the question: How is the data compiled in the JSON file? Is there one person behind the JSON file writing it all or is it automatic? For our miniX08 we just gained access to an online API that PokéAPI contributors are in charge of. However it would be interesting to research who these people are and how they take the data from the game and paste it into the JSON file. In addition to this it would also be interesting to investigate further how easy it would be to manipulate such files? E.g. If there was only one person behind this API, then this person would have access to easily manipulate the data from the game.


#### Reference:
[PokéAPI](https://pokeapi.co)

<br>

[Aesthetic programming chapter 8](https://aesthetic-programming.net/pages/8-queery-data.html)

<br>

[Pokemon card maker](https://pokecardmaker.net/creator)

<br>

[Horoscope](https://www.zodiacsign.com)

<br>

[Pokecard maker](https://pokecardmaker.net/creator)

<br>

[Pokemon zodiac signs](https://dk.pinterest.com/pin/754001162592450077/)

<br>

[Reference for eyeshape](https://www.artstation.com/artwork/8yOAq)

<br>

ChatGPT
