let colors = ['white', 'black', 'red', 'orange', 'green', 'lightblue', 'turquoise', 'blue', 'lightpink'];
let currentColorIndex = 0; //A variable to keep track of the current index in the colors array.
let wasMousePressed = false; //A boolean variable to track whether the mouse was pressed or not.
let img;

function preload() {
  // Load the image before setup
  img = loadImage('moodcolor.png');
}

function setup(){
  createCanvas(windowWidth, windowHeight);

}

function draw(){
  background(173, 216, 230);

  // Setting the cursor to hand: 
  cursor(HAND);

   // Headline:
   textSize(65);
   fill(255);
   stroke(0);
   strokeWeight(5);
   textStyle(BOLDITALIC);
   text('ColorMoodScale', 450, 100);

  // Phone and screen where the emoji is created:
  //Phone:
  fill(100);
  stroke(1);
  strokeWeight(2);
  rect(800, 240, 260, 400, 15, 15, 15, 15); 
  fill(255, 150);
  noStroke();
  rect(910, 255, 50, 6, 20); 
  ellipse(885, 258, 11, 11);
  ellipse(860, 258, 11, 11);
  ellipse(975, 258, 6, 6);
  fill(255);
  noStroke();
  rect(1061, 325, 4, 50, 6);
  rect(1061, 400, 4, 25, 6);
//Screen:
  fill(200);
  stroke(1);
  rect(815, 285, 230, 330, 15, 15, 15, 15);
//Text inside:
  let s="CREATE ME"
  stroke(1);
  textSize(17);
  fill(255);
  text(s, 876, 315);

  // Textbox with information:
  fill(235);
  stroke(1);
  rect(270, 240, 325, 400, 15, 15, 15, 15);
//Text inside:
  let h="Information"
  stroke(1);
  textSize(25);
  fill(173, 216, 230);
  text(h, 362, 280);

//Little textbox 1:
  fill(0, 106, 255);
  stroke(0, 125, 255);
  rect(295, 325, 150, 50, 20);
//Text inside the little textbox 1:
  let t0="Hi! I'm Emoji."
  noStroke();
  textSize(12);
  textStyle(NORMAL);
  fill(255);
  text(t0, 320, 355);
  
  //Little textbox 2:
  fill(0, 106, 255);
  stroke(0, 125, 255);
  rect(295, 390, 275, 50, 20);
//Text inside the little textbox 2:
  let t1="Press on me and make me feel like you do today."
  noStroke();
  textSize(12);
  textStyle(NORMAL);
  fill(255);
  text(t1, 306, 420);

//Little textbox 3: 
  fill(0, 106, 255);
  stroke(0, 125, 255);
  rect(295, 460, 265, 165, 20);
//Text inside the little textbox 3:
  let t2="ColorMoodScale can be seen below:"
  noStroke();
  textSize(12);
  textStyle(NORMAL);
  fill(255);
  text(t2, 315, 490);
// Display the image inside the textbox 3:
image(img, 390, 495, 70, 120);


//Creating the start emoji:
  //The color of the emoji's face changes based on the currentColorIndex, cycling through the colors array.
  fill(colors[currentColorIndex]);
  noStroke();
  circle(930, 400, 140);

//Eyes:
  fill(255);
  ellipse(908, 385, 25, 40);  
  ellipse(950, 385, 25, 40);
//Inside the eye left 
  fill(0);
  stroke(255, 20);
  strokeWeight(2);
  ellipse(908, 390, 15, 18);
  fill(255);
  ellipse(910, 395, 6, 7);
//Inside the eye right
  fill(0);
  stroke(255, 20);
  strokeWeight(2);
  ellipse(951, 390, 15, 18);
  fill(255);
  ellipse(953, 395, 6, 7);

  //Eyebrows - making the left eyebrow raise if mouse is pressed
  // If the mouse is pressed, the emoji's color changes to the next color in the array.
  if (mouseIsPressed) { // raises the left eyebrow
    noFill();
    stroke(0);
    strokeWeight(4);
    curve(878, 378, 898, 348, 918, 348, 938, 378); // left eyebrow
    curve(920, 378, 940, 358, 960, 358, 980, 378); // right eyebrow
  } else {// standard position of the brows
    noFill();
    stroke(0);
    strokeWeight(4);
    curve(878, 378, 898, 358, 918, 358, 938, 378); // left eyebrow
    curve(920, 378, 940, 358, 960, 358, 980, 378); // right eyebrow
  }

  // Change the color when mouse is pressed
  if (mouseIsPressed && !wasMousePressed) {
    // Increment the currentColorIndex to cycle through colors
    currentColorIndex = (currentColorIndex + 1) % colors.length;
    wasMousePressed = true;
  } else if (!mouseIsPressed) {
    wasMousePressed = false;
  }

}
