//Creating 3 arrays with string as value - list of the words the computer loads. Overthinking thoughts from the computer. 
let firstWordLine = ['What should I do?', 'Loading', 'Oh, no they are waiting on me', 'Overthinking now', 'Idk', 'Processing', 'Downloading', 'Wait, did they do now?', 'It is taking forever'];
let secondWordLine =["Be faster!", "Come on", "Thinking", "Stress", "Image.png", "Fetching", "p5.js", "Managing", "Wait What?", "word.length", "Did I get it right?", "Now or never"];
let thirdWordline = ['What should I do?', 'Loading', 'Oh, no they are waiting on me', 'Overthinking now', 'Idk', 'Processing', 'Downloading', 'Wait, did they do now?', 'It is taking forever'];

let angle = 0;

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(10); //making draw loop it scope 10 times pr second (making the program appear faster)


}
   
function draw() {  
//setting up backgroundcolor:
background(200, 50, 95);


//Making multiple lines of text
  //looks like throught the computer is having with itself when the throbber is loading 
//Running as a background
  push() //save the current drawing style and transformations 
  fill(255, 100); //set the fill color 
  textSize(40); //set the text size 
  noStroke(); 
  for (let i = 0; i < 20; i++) { // loop runs 20 times and display a random element from each of the three arrays
    //the y poistion changes with each iteration and x remains constant //floor used to run up or down to a even number (random() is oneven)
    text(firstWordLine[floor(random(firstWordLine.length))], 5, 50*i);
    text(secondWordLine[floor(random(secondWordLine.length))], 545, 50*i);
    text(thirdWordline[floor(random(thirdWordline.length))], 910, 50*i);
  }
  pop()

//Creating the headline
  textSize(45);
  fill(255);
  stroke(1);
  strokeWeight(4);
  text('HOLD ON, LET ME OVERTHINK THIS...', 300, 200);



// Now I want to make a rectangle, with rotates
   push() // Push() and pop() makes a bubble to isolate styles and transformation, so it does not effect the rest of the code
   // another word for saving, remembering the rotation and the origin punkt. i start by moving the origin to the midt of my canvas, thats why i devide the height and width with 2
   translate(width/2,height/2);
   for (let i=0; i<100; i++){
     // i tell the for-loop that i is 0 and i has to be smallere that 100. 
     //I add rectangle everytime it runs, it add a rect in the last part of the for-loop

   rotate(angle);
   scale(0.95);
     // I picked to set the scale to 0.95, whichs means that everytime a rect is being added, the rect is 5% smallere
 
 //here I add/created the rectangle
     rectMode(CENTER);
     fill(15, 100, 255, 150);
     rect(0, 0, 300, 300);
   }

    //This is where I give my rotation a speed:
    angle += 0.012;
  
    pop() // reseting to the original environment 
 
}
   