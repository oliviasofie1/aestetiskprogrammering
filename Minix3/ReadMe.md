## MiniX3 - Infinite loops - "The OverThinker"

[_Click to view my **MiniX3**_](https://oliviasofie1.gitlab.io/aestetiskprogrammering/Minix3)

[_Click to view my my **code**_](https://gitlab.com/oliviasofie1/aestetiskprogrammering/-/blob/main/Minix3/MiniX3.js)


### **My Throbber design:**
I would describe a throbber as an animated icon that indicates to the user that a process is un its way, such as loading content or performing an action. 

In this MiniX project, our task was to create and design a throbber, a visual representation of an ongoing process within a system. My concept for the throbber was inspired by a common experience many people, including myself, face: overthinking. I aimed to create a throbber that not only captured the essence of a typical loading indicator but also incorporated elements related to the mental process of overthinking. To achieve this, I envisioned a throbber that was vibrant and visually captivating, using colors and motions to make it interesting to observe. 

Additionally, I wanted the throbber to reflect the complexities of human thought processes, particularly those that occur during periods of overthinking or deep contemplation.
Essentially, I set out to create a "human" throbber—one that not only indicated a process but also hinted at the complex human mind. I this way, I want people think about the throbber as a piece, that can start a reflection in themself, and maybe make them forget about the time.  Instead of people waiting, looking at a plain throbber, while being annoyed because it takes time on loading. This concept guided my design process as I brainstormed ways to visually represent the nuances of thought and reflection within the confines of a throbber.

You can see the result of my throbber:

![](throbber.mp4)


In my program there can be seen a throbber, which is a rotating rectangle, that creates a from of flower. Other than that, a visual representation of overthinking thoughts, that the computer is having. The thoughts is represented by overthinking words scrolling vertically on the canvas.


### **What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?:**
The time-related function , that I have used to create the throbber, is primary the rotate function, which rotates the rectangles, and in this way creating a never-ending loop. By incrementing the rotation angle over time, I create the illusion of dynamic movement in my animation. This function manipulates time in computation by controlling the transformation of shapes over successive frames, but the element that gives the function its time-related abilities is the draw function combined with, I my case, angle. The draw function is in itself a time-related function since it runs in a loop and in my program it re-runs the code 10 times per second, which mean that it's continuously updating the display. This loop is crucial for creating dynamic animations, as it allows user to update the screen content in response to changing variables or user input over time. When rotate is set to rotate(angle), then there need to be added a value to the angle in order for it to move. If these requirements are met, the object will move, as seen in my RunMe. Other then that, I have used a for-loop, which is displaying three arrays made of strings. These are running in multiple lines of text at different positions on the screen. The loop runs 20 times per frame, which meant to look like a swarms of thoughts, that the computer is having with itself, when the throbber is running. 

The think I wanted to focus on, in the making of my throbber, was to create a visual symbol of technological processing, but in another way as it is normally looking like. I wanted to create a throbber and a program, which illustrates that times flies, when we are overthinking about things. I wanted to create an experience, where the user can get behind the throbber and see, how it would be if the computer was a real human with overthinking thoughts about everything it do. Because then maybe , then we wouldn't be irritated about the "long" loading time, but maybe instead think that it is okay, if it needs sometime to loading and think - actually like us human need sometimes.

In the creation of all this, I got a lot of inspiration from text:  “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation by Lammerant, because he describes, how emotions are being dependent of human exportations linked to the experience of time presence. This is one of the things, that I wanted to point out with the throbber and program that I have created. In this way, I hope and wish, that my feeling that the throbber is giving the user is another experience of time, because it should get the user interesting in, what is going on and maybe get them reflection and rises question like: what is going on and what does it mean? I hope, that this happens, because as Lammerant describes, actual experience of time is shaped and strongly influenced by all sort of design decisions and implementations.


### **Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?:**
So for me a throbber is an animated icon that indicates to the user that a process is on its way, such as loading content or performing an action. It's a form of waiting icon, which show the user the time it takes to perform the tasks, that are necessary to connect the computer to different servers. In this way, the throbber is a form for entertainment for the user, and therefore the user don't need to see everything that's happening being and in the background of the throbber. 

When I started on this assignment and got to think about throbbers, then I discovered that I see them nearly every single day in different digital platforms. This could for example be on Instagram, when I'm waiting for the latest posts, but also when I am waiting for my e-mail to download. These throbbers are looking a lot like each other, in the way that they are boring and not so interesting to look at, which can make the waiting time a bit annoying. 

A throbber that is playing a bit more with the entertainment is the SnapChat throbber, which is a little ghost. The ghost throbber is the same as the logo for SnapChat. The throbber is having funny features, such as the more you pull down the screen the longer gets the ghost. In this way, it makes the waiting time feel shorter, because it can make people occupied and interested, this I also hope my throbber can do. 


### **Reference of what I used:**
- https://p5js.org/reference/#/p5/let
- https://p5js.org/reference/#/p5/fill
- https://p5js.org/reference/#/p5/stroke
- https://p5js.org/reference/#/p5/if-else
- https://p5js.org/reference/#/p5/text
- https://p5js.org/reference/#/p5/background
- https://p5js.org/reference/#/p5/noStroke
- https://p5js.org/reference/#/p5/rect
- https://p5js.org/reference/#/p5/textSize
- https://p5js.org/reference/#/p5/push
- https://p5js.org/reference/#/p5/pop
- https://p5js.org/reference/#/p5/frameRate
- https://p5js.org/reference/#/p5/translate
- https://p5js.org/reference/#/p5/rotate
- https://p5js.org/reference/#/p5/scale
- https://p5js.org/reference/#/p5/rectMode

### **Reference:**
Hans Lammerant. (2018) “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation.

Executing.  Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press
