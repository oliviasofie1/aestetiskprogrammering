//Global variables:
let computerVaccineImageSize = {
  w: 175,
  h: 175
}
let computerVaccineImage;
let computerVaccineImagePosX;
let screen = 0;
let score = 0;
let vaccines = []; //array where vaccine objects will be push inside 
let vaccineImg;
let min_vaccines = 5;
let x = 200;
let mySound;

function preload(){ //loading my images - runs before the rest of the code - ready to be used:
  vaccineImg = loadImage('vaccineImg.png');
  computerVaccineImage = loadImage('computerVaccineImage.png');
  computerVirusImage = loadImage('computerVirusImage.png');
  vaccineRoomImage = loadImage('vaccineRoom.png');
  hospitalImage = loadImage('hospital.jpg');
  mySound = loadSound('virusSong.mp3');
}

function setup(){
 createCanvas(1200, 700);
 textAlign(CENTER);
 computerVaccineImagePosX = 600;
}


function draw(){
//creating conditional statements/if-statements, which check the value of the variable called screen.
    //if the screen is equal to 0, then it calls the startScreen() function
    //if the screen is equal to 1, then it calls the gameOn() function
    //if the screen is equal to 2, then it calls the endScreen() function
  if(screen == 0){
    startScreen();
  }else if (screen == 1){
    gameOn();
  }else if(screen == 2){
    endScreen();
  }

}

//creating function startScreen, which comes up before the game starts
function startScreen(){
  background(hospitalImage);
  fill(255, 31, 53);
  stroke(255);
  strokeWeight(4);
  textSize(45);
  textAlign(CENTER);
  text('Are you ready to play CyberGuard: Digital Vaccination', width/2, height/3);
  text('Click to start and protect your computer', width/2, height/2);
  reset(); // Calls the reset function
  
}

//creating function gameOn, which comes up, when the game starts. 
function gameOn(){
  background(vaccineRoomImage);
  fill(255, 31, 53);
  textSize(25);
  text("score = " + score, width/2, 690);
  textSize(15);
  text("Move the mouse to vaccinate the computer", width/2, 670);
  textSize(40);
  text("CyberGuard: Digital Vaccination", width/2, 130);
  push(); //using the function push, so the image computerVirus is getting loaded:
    //The function push saves the current drawing style settings and transformations onto a stack. 
  imageMode(CENTER);
  //Getting the "computerVaccineImage" to follow the mouseX movement.
  image(computerVaccineImage, mouseX, height-120, computerVaccineImageSize.w, computerVaccineImageSize.h);
  pop(); // reseting to the original environment 

  //calling the different functions
  checkVaccineNum(); 
  showVaccine();
  checkCatching();
  checkVaccineMove();

}

//Function if there is 5 object on the screen
function checkVaccineNum(){
  //if-statement checking if as many vaccines object gets shown on the screen as the value of the variable min_caccine
  if(vaccines.length < min_vaccines){
    vaccines.push(new Vaccines()); //To create a class instance we use the new keyword which create a instance of a class and then calls the constructor method on that object - and pushs the vaccines out
  } //every time their is less then min_vaccines on the screen a new object gets push out - class the constructor method on the object 
}

//Function check if there is any object in the array - dot.notation sets method on each object in array
function showVaccine(){
  //for-loop running through vaccine:
  for(let i = 0; i < vaccines.length; i++){
    //assigning every vaccine object with the method move and show - for every loops running
      //making sure it gets the right behaviors
    vaccines[i].move();
    vaccines[i].show();
  }
}

//checks if any vaccine in the vaccines array has moved beyond the bottom of the screen (height)
  //If true = changes the value of screen to 2 - gameover
function checkVaccineMove(){
  for (let i = 0; i < vaccines.length; i++){
    if(vaccines[i].pos.y > height){
      screen = 2;
    }
  }
}


//function checking if the computerVaccineImage have catched the object vaccine.
function checkCatching() {
  for (let i = 0; i < vaccines.length; i++) {
    let d = int(dist(mouseX, 500, vaccines[i].pos.x, vaccines[i].pos.y)); //checks the distance between the mouse position and each vaccine object
    if (d < computerVaccineImageSize.w / 2) {
      score += 1; //increments score
      vaccines.splice(i, 1); //Removes the caught vaccine from the 
      print("hallo");
    }
  }
}

//creating function endScreen, which comes up, when the game ends // code inside gets execute when function get called
function endScreen(){
  background(173, 215, 230);
  image(computerVirusImage, width/8, height/8, 800, 600);
  stroke(255);
  strokeWeight(4);
  textSize(45);
  textAlign(CENTER);
  fill(255, 0, 0);
  textSize(45);
  text('Your computer got infected by virus', width/2, height/8);
  textSize(20);
  text('Click to play again', width/2, height/5);

}

//This function will be called when the mouse is pressed. making a function mousePressed with if statements to check if the mouse have been click:
function mousePressed(){
  //If screen is equal to 0 (startScreen is currently displayed)
  if(screen==0){
    screen = 1; //true = gameScreen gets called
    mySound.play(); 
  //If screen == 2 (endScreen is currently displayed). 
  }else if(screen==2){
    vaccines.splice(0, vaccines.length) //clearing the vaccine array with splice 
      screen=0; //value of screen = 0 - restarts game (startScreen)
      mySound.stop();
  }
}

//The reset() function = resetting certain objects to their initial values.
function reset() {
  score = 0;
  for (let i = 0; i < vaccines.length; i++) {
    vaccines[i].pos.y = 0; //y = 0 of the object vaccine after resetting the game
  }
}


