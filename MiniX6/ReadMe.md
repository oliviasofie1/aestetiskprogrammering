### Object Abstraction - "CyberGuard: Digital Vaccination"
#### MiniX6

[_Click to view my **MiniX6**_](https://oliviasofie1.gitlab.io/aestetiskprogrammering/MiniX6)

[_Click to view my my **code: MiniX6.js**_](https://gitlab.com/oliviasofie1/aestetiskprogrammering/-/blob/main/MiniX6/MiniX6.js?ref_type=heads)

[_Click to view my my **code: vaccine.js**_](https://gitlab.com/oliviasofie1/aestetiskprogrammering/-/blob/main/MiniX6/vaccine.js?ref_type=heads)


#### How does my game and object work?:
In the beginning, I had many ideas about, how I wanted to create the game, and what type of could make. I really fast realized, that there is a lot of potential in a game. In the way, that there are so many different and nearly endless kinds of features, that you can add to a game. I decided, that I wanted to make an aesthetic game, that work with the abstraction of object-oriented programming (OOP). Abstraction allows me to focus on, what an object does rather, than how it does it, which makes the code way more reusable, manageable and easier to understand.

In this way, for my MiniX6 I created a game called "CyberGuard: Digital Vaccination". The inspiration for the program came to me, because I was thinking back to the covid-19 pandemic, which make me think about, that computer, like humans, can get a virus. Therefore, I wanted to created a game, where a computer needs a vaccine against a virus, like humans need. I wanted to play with the idea, that the computer is getting human, because it needs to get vaccinated to stay a healthy and not become "sick".

I decided to name the game "CyberGuard: Digital Vaccination", because this name captures the essence of the game, where a computer, personified as a sentient entity, which needs a vaccine to protect itself from a software virus, mirroring the concepts of human vaccination against illnesses. The term "CyberGuard" emphasizes the cybernetic aspect of the game, while "Digital Vaccination" conveys the process of safeguarding the computer from digital threats.  

Before I started programming the game,  I wanted to make some rules, which could help me when I started coding the game, so I knew, what kind of game I wanted to create.. The rules can be seen below:

Rules for the game "CyberGuard: Digital Vaccination":
1. The player need to click on the mouse to start the game. The screen will then switch from the start screen to the game scree.
2. Use the mouse to move the computer, so player can catch the vaccines to make sure the computer is not getting a virus.
3. The game needs to check, if each of the vaccine objects has been caught or wasted. 
4. When one vaccine is not caught, then the game ends and the computer, in the game, has got a virus. The screen will then switch from the game screen to the end screen. 
5. To restart the game the player need to click on the mouse.

As explained in the rules, the game works, when the player click on the mouse, which gets the game started. Now vaccines are falling down, which can save the computer from a software virus. The player needs to catch vaccines, the objects, with the computer, but if it does not catch the vaccine then it will get infected by a virus. If the player wants to restart the game, then the mouse must be clicked. As an extra element, I decided to add/load a sound/song to the game called "virusSong". I picked to add a sound as a fun and exciting element in the game, so the player can get into the feeling of playing a game. The song is fitting to the theme, which is also helping to get people into the mood.


You can see the result of the game here:
![](MiniX6-picture1.png)

![](MiniX6-picture2.jpeg)

![](MiniX6-picture3.png)


#### How I program the objects and their related attributes, and methods in my game?:
I decided to create a simple game with dynamic interactions and viusal feedback. To program the objects and their related attributes and methods in my game, I defined attributes for the computer vaccine, such as its position (computerVaccinePosX), size (computerVaccineSize), and images (computerVaccine, vaccineImg) ect. Other then this, instantiated objects using these attributes. For example, computerVaccine is instantiated with its position set to computerVaccinePosX. 

I implemented a class for the purpose of encapsulating the idea of data and functionality into an object,  since it's an object-oriented program. Therefore, I implemented class Vaccines to represent the vaccine objects. This class encapsulates attributes like size, position and speed, as well as methods like move() and show() to handle movement, behavior and rendering. I am managing multiple vaccine objects using an array called vaccines, which i declared in the start program (let vaccines = [];), and iterate over them to update their positions and display them on the canvas. 

To handle interaction, I implemented methods, such as checking if the computer vaccine catches the vaccine objects (checkCathing()), and updating the score accordingly. 

Other then this, I have implemented the function to handle mouse clicks using the "mousePressed() function. This function is working as an event listener, which get called whenever the mouse button is pressed by the player. Inside the function I have been using if-statements to handle different scenarios based on the current value of the screen variable. If the screen is equal to 0 it means the start screen is displayed. If the it's equal to 1 it transitions to the game screen and I play a sound using  mySound.play(). Lastly if the screen is equal to 2 it means the end screen is displayed. In this case, I clear the vaccine array using vaccines.splice(0, vaccines.length) to remoce all vaccine object and set the screen back to 0. 

I decided to organizing my game to different game states. I organized the game into screens. I have functions called startScreen(), gameOn() and endScreen(), to handle different screens and transitions between them based on the value of screen. 

Lastly, I have provided a reset() function, which is responsible for resetting certain variables to their initial values when called. Inside the function I have reset the score to 0 and loop through all vaccine objects in the vaccine array, resetting their, pos.y attributes to 0 to set them back to their initial position at the top of the screen.


#### What are the characteristics of object-oriented programming and the wider implications of abstraction?:
The key characteristics of OOP is properties and behaviors, which comes from one of the concepts of OOP called abstraction. Which Soon and Cox define in the book: 

“The main goal is to handle an object’s complexity by abstracting certain details and presenting a concrete model.”

The useful things about this is that you can customize elements of the code in a more manageable and reusable, which makes the code easier to read as well. By creating classes and what you put into these classes applies through the entire code, which helps to decrease the complexity, because you can just refer to the class. The class is full with all, that make them work in the program. 

An other things, about OOP is that the same token, can be reuse the same properties and behaviors to create another object with the corresponding data values. This means that all objects have same methods to be described, but they have slightly different outcome. This is the same of humans. 

Overall, Object-oriented programming (OOP) emphasizes organizing code into objects with data and behavior. Abstraction, is a key principle in OOP, simplifies complexity by hiding unnecessary details, making code easier to understand and maintain. This abstraction fosters code reusability, promotes collaboration among developers, and enhances scalability, ultimately leading to more efficient and adaptable software systems. In this way, abstraction is the concept of representing essential features without including the background details. It allows programmers to focus on what an object does rather than how it does it.


#### Connect my game project to a wider cultural context, and think, of an example to describe how complex details and operations are being "abstracted"?:
The concept of vaccinating a computer against viruses mirrors real world scenario of us humans getting vaccinated against diseases. By personifying the computer, as a sentient entity that needs protection from the digital threats. I hope to draw some parallels between the biological and digital world. Because this is highlighting the increasing integration of technology into our daily lives but in the same time underscoring the importance of cybersecurity and digital hygiene in modern world. 

The essence of abstraction in my game lies in, how I encapsulated the functionalities of the vaccines and the computer. These two I abstracted them into meaningful objects with defined behaviors. As an example I defined attributes, like position and size, for the vaccines and the computer, as well as methods like show() and move() to handle their rendering and movement. This abstraction allowed me to focus on, what the objects do - catching vaccines and avoiding viruses - rather than how they do it, which make the code more manageable and easier to understand.

The focus in my game was not only to serve as a engaging and fun experience but also to offer insights into broader cultural themes, such as cybersecurity and the parallels between the computer (digital) and humans (biological) immunity. 


#### Reference:
Soon Winnie & Cox, Geoff, "Auto, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164

https://p5js.org/examples/objects-objects.html

https://p5js.org/examples/objects-array-of-objects.html


