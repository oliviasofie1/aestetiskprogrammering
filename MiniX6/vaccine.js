//This class represents the vaccine objects in your game. It includes a constructor to initialize the properties of each vaccine object and methods to move and display the vaccine.
//creating a class: template/blueprint of objects with properties and behaviors
//Vaccine class constructor sets the initial position when a new vaccine object is created. 
class Vaccines { 
  constructor(){ //initalize the objects
    this.speed = floor(random(1, 6)); //setting the speed of the vaccines 
    this.pos = new createVector(random(0, width), 0); //setting the positons of the vaccines
    this.size = floor(random(75, 95)); //setting the size of the vaccines
  }

  move(){ //moving behaviors using the methods move
    //+= is a opeators 
     // Update the y position to make the vaccine move downward
    this.pos.y += this.speed;

  }

  show(){
    // Draw the vaccine image by using the methods show:
    image(vaccineImg, this.pos.x, this.pos.y, this.size, this.size);
  }
}



